import Vue from "vue";
import App from "./App.vue";
import axios from "axios";
import VueAxios from "vue-axios";

Vue.config.productionTip = false;

const client = axios.create({
  // Option 1: Use the .env files to distinguish the production vs. development
  // endpoints.
  // baseURL: process.env.VUE_APP_API_BASE_URL,
  //
  // Option 2: Use the same endpoint, with the Vue dev server transparently
  // proxying to the real API URL.
  baseURL: "/api/v1",
});
Vue.use(VueAxios, client);

new Vue({
  render: (h) => h(App),
}).$mount("#app");
