// Option 2: Proxy all traffic starting with "/api" to http://localhost:8081
module.exports = {
  devServer: {
    proxy: {
      "^/api": {
        target: "http://localhost:8081",
        changeOrigin: true,
      },
    },
  },
};
