package test

import (
	"encoding/json"
	"errors"
	"io/ioutil"
	"path"
	"runtime"

	"github.com/qri-io/jsonschema"
)

var (
	ErrPathDiscovery = errors.New("could not identify test objects path")
)

func GetPetSchema() (*jsonschema.Schema, error) {
	_, filename, _, ok := runtime.Caller(0)
	if !ok {
		return nil, ErrPathDiscovery
	}

	path := path.Join(path.Dir(filename), "..", "/api", "pet-schema.json")

	b, err := ioutil.ReadFile(path)
	if err != nil {
		return nil, err
	}

	rs := &jsonschema.Schema{}
	if err := json.Unmarshal(b, rs); err != nil {
		return nil, err
	}

	return rs, nil
}
