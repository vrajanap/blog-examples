package petstore

import (
	"encoding/json"
	"errors"
	"fmt"
	"strings"
)

var (
	ErrValidationFail = errors.New("validation failure")
)

type Pet struct {
	ID   int64  `json:"id"`
	Name string `json:"name"`
	Tag  string `json:"tag,omitempty"`
}

func ReadPet(b []byte) (*Pet, error) {
	f := &Pet{}

	if err := json.Unmarshal(b, f); err != nil {
		return nil, err
	}

	return f, nil
}

func (p *Pet) Validate() error {
	if p.ID < 0 {
		return fmt.Errorf("%w: 'id' must be greater than 0", ErrValidationFail)
	}

	if p.Name == "" {
		return fmt.Errorf("%w: 'name' must not be empty", ErrValidationFail)
	}

	if len(p.Name) > 100 {
		return fmt.Errorf("%w: 'name' must be less than 100 characters", ErrValidationFail)
	}

	if p.Tag != "" {
		validTags := []string{
			"cat",
			"dog",
		}
		hasValidTag := false

		for _, t := range validTags {
			if p.Tag == t {
				hasValidTag = true
			}
		}

		if !hasValidTag {
			return fmt.Errorf("%w: 'tag' must be one of the following: [%s]",
				ErrValidationFail, strings.Join(validTags, ","))
		}
	}

	return nil
}
