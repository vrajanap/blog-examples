package subpackage_test

import (
	"testfixtures/test"
	"testing"
)

func TestSubpackage(t *testing.T) {
	_, err := test.GetPetSchema()
	if err != nil {
		t.Fatal(err)
	}
}
