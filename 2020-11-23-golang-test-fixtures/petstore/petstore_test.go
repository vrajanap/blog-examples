package petstore_test

import (
	"io/ioutil"
	"strings"
	"testing"

	"testfixtures/petstore"
)

func TestReadPet_Inline(t *testing.T) {
	j := `
	{
		"id": 42,
		"name": "Foo",
		"tag": "dog"
	}
	`

	result, err := petstore.ReadPet([]byte(j))
	if err != nil {
		t.Errorf("error while unmashalling: %v", err)
	}

	if result.ID != 42 && result.Name != "foo" {
		t.Errorf("%v: output doesn't match expected result", result)
	}
}

func TestReadPet_TestData(t *testing.T) {
	b, err := ioutil.ReadFile("testdata/sample.json")
	if err != nil {
		t.Fatal(err)
	}

	result, err := petstore.ReadPet(b)
	if err != nil {
		t.Errorf("error while unmashalling: %v", err)
	}

	if result.ID != 42 && result.Name != "foo" {
		t.Errorf("%v: output doesn't match expected result", result)
	}
}

func TestValidate_NoBuilder(t *testing.T) {
	tests := []struct {
		name    string
		pet     *petstore.Pet
		wantErr bool
	}{
		{
			name: "Validates a valid pet",
			pet: &petstore.Pet{
				Name: "Foo",
				ID:   64,
				Tag:  "dog",
			},
			wantErr: false,
		},
		{
			name: "Validates if tag is empty",
			pet: &petstore.Pet{
				Name: "Foo",
				ID:   64,
				Tag:  "",
			},
			wantErr: false,
		},
		{
			name: "Errors if ID less than 0",
			pet: &petstore.Pet{
				Name: "Foo",
				ID:   -200,
				Tag:  "dog",
			},
			wantErr: true,
		},
		{
			name: "Errors if name is empty",
			pet: &petstore.Pet{
				Name: "",
				ID:   64,
				Tag:  "dog",
			},
			wantErr: true,
		},
		{
			name: "Errors if name is greater than 100 characters",
			pet: &petstore.Pet{
				Name: strings.Repeat("A", 101),
				ID:   64,
				Tag:  "dog",
			},
			wantErr: true,
		},
		{
			name: "Errors if tag is invalid",
			pet: &petstore.Pet{
				Name: "Foo",
				ID:   64,
				Tag:  "turkey",
			},
			wantErr: true,
		},
	}

	for _, tt := range tests {
		tt := tt
		t.Run(tt.name, func(t *testing.T) {
			if err := tt.pet.Validate(); (err != nil) != tt.wantErr {
				t.Errorf("error = %v, wantErr = %v", err, tt.wantErr)
			}
		})
	}
}

func getTestPet(options ...petOption) *petstore.Pet {
	p := &petstore.Pet{
		Name: "Foo",
		ID:   42,
		Tag:  "dog",
	}

	for _, option := range options {
		option(p)
	}

	return p
}

type petOption func(*petstore.Pet)

func withTag(tag string) petOption {
	return func(p *petstore.Pet) {
		p.Tag = tag
	}
}

func withName(name string) petOption {
	return func(p *petstore.Pet) {
		p.Name = name
	}
}

func withID(id int64) petOption {
	return func(p *petstore.Pet) {
		p.ID = id
	}
}

func TestValidate_Builder(t *testing.T) {
	tests := []struct {
		name    string
		pet     *petstore.Pet
		wantErr bool
	}{
		{
			name:    "Validates a valid pet",
			pet:     getTestPet(),
			wantErr: false,
		},
		{
			name:    "Validates if tag is empty",
			pet:     getTestPet(withTag("")),
			wantErr: false,
		},
		{
			name:    "Errors if ID less than 0",
			pet:     getTestPet(withID(-200)),
			wantErr: true,
		},
		{
			name:    "Errors if name is empty",
			pet:     getTestPet(withName("")),
			wantErr: true,
		},
		{
			name:    "Errors if name is greater than 100 characters",
			pet:     getTestPet(withName(strings.Repeat("A", 101))),
			wantErr: true,
		},
		{
			name:    "Errors if tag is invalid",
			pet:     getTestPet(withTag("turkey")),
			wantErr: true,
		},
	}

	for _, tt := range tests {
		tt := tt
		t.Run(tt.name, func(t *testing.T) {
			if err := tt.pet.Validate(); (err != nil) != tt.wantErr {
				t.Errorf("error = %v, wantErr = %v", err, tt.wantErr)
			}
		})
	}
}
